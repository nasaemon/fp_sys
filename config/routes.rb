Rails.application.routes.draw do
  root to: 'top#index'

  devise_for :customers
  devise_for :planners

  resources :reservable_frames, only: %i[index create destroy]
  resources :planners, only: %i[index] do
    resources :reservations, shallow: true
  end
  get 'mypage' => 'mypage#index'
end
