# README
## heroku url
https://fpsys.herokuapp.com/

## Required Software
* Ruby 2.5.0
* Ruby on Rails 5.2.0
* MySQL 5.7

## Setup
### .envの配置
.env.sampleを参考に各種変数を設定し、.envとして配置してください。
#### 変数説明
* MYSQL_ROOT_PASSWORD
  * MySQLのrootパスワードです。

### pre-commit
pre-commitを使うことで、スタイルチェックなどをcommit前にやってくれます。

```
$ bundle exec pre-commit install
```

## Development with Docker
### Getting Started
1. Dockerが必要です。`docker`, `docker-compose`, `docker-machine`のコマンドが入っているか確認してください。

```
$ docker --version
$ docker-compose --version
$ docker-machine --version
```

2. ビルドしてください。

```
$ docker-compose build
```

3. データベースを作成する

```
$ docker-compose run web rails db:setup
```

4. アプリを実行します。

```
$ docker-compose up
```

デタッチドモードで実行させたいときは、

```
$ docker-compose up -d
```
