FROM ruby:2.5.0

RUN apt-get update -qq && \
    apt-get install -y build-essential libpq-dev apt-transport-https locales && \
    rm -rf /var/lib/apt/lists/*

# for yarn
ENV NODE_VERSION 8
RUN curl -sSL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash - && \
    apt-get install -y nodejs && \
    rm -rf /var/lib/apt/lists/*
RUN curl -sSL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update -qq && \
    apt-get install -y yarn && \
    rm -rf /var/lib/apt/lists/*

# locale
RUN echo "ja_JP.UTF-8 UTF-8" > /etc/locale.gen && \
    dpkg-reconfigure --frontend=noninteractive locales && \
    update-locale LANG=ja_JP.UTF-8
ENV LANG ja_JP.UTF-8
ENV LANGUAGE ja_JP:en
ENV LC_ALL ja_JP.UTF-8

ARG appdir=/fp_sys
RUN mkdir $appdir
WORKDIR $appdir

COPY package.json yarn.* ./
RUN yarn

COPY Gemfile ./
COPY Gemfile.lock ./
RUN bundle install -j4 --path /bundle

COPY . ./
