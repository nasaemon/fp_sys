class ChangeLimitNameToUsers < ActiveRecord::Migration[5.2]
  def up
    change_column :users, :name, :string, limit: 50
  end

  def down
    change_column :users, :name, :string, limit: 255
  end
end
