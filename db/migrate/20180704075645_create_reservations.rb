class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.references :customer, foreign_key: { to_table: 'users' }, null: false
      t.references :reservable_frame, index: { unique: true }, foreign_key: true, null: false

      t.timestamps
    end
  end
end
