class AddUniqueIndexToReservableFrame < ActiveRecord::Migration[5.2]
  def change
    add_index :reservable_frames, %i[date planner_id time_frame_id], unique: true
  end
end
