# frozen_string_literal: true

class ChangeLimitTypeToUsers < ActiveRecord::Migration[5.2]
  def up
    change_column :users, :type, :string, limit: 30
  end

  def down
    change_column :users, :type, :string, limit: 255
  end
end
