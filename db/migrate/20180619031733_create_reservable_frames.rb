# frozen_string_literal: true

class CreateReservableFrames < ActiveRecord::Migration[5.2]
  def change
    create_table :reservable_frames do |t|
      t.references :time_frame, null: false
      t.references :planner, null: false
      t.date :date, null: false

      t.timestamps
    end
  end
end
