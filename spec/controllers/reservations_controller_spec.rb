# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReservationsController, type: :controller do
  let(:customer) { FactoryBot.create(:customer) }
  let(:reservable_frame) { FactoryBot.create(:reservable_frame) }
  let(:params) { { planner_id: reservable_frame.planner.id, reservation: { reservable_frame_id: reservable_frame_id } } }
  let(:reservable_frame_id) { reservable_frame.id }

  describe 'GET #index' do
    subject(:response) { get :index, params: { planner_id: reservable_frame.planner.id } }

    it { expect(response.status).to eq(200) }
  end

  describe 'POST #create' do
    before do
      @request.env['devise.mapping'] = Devise.mappings[:customer]
      sign_in customer
    end
    subject(:response) { post :create, params: params }

    context 'when reservable_frame_id is not nil' do
      it { expect(response.status).to eq(200) }
      it { expect { response }.to change(Reservation, :count).by(1) }
    end

    context 'when reservable_frame_id is not nil' do
      let(:reservable_frame_id) { nil }
      it 'is expected to return 422 with error message' do
        expect(response.status).to eq(422)
        expect(response.body).to be_json_eql('["Reservable frame must exist"]')
      end
      it { expect { response }.not_to change(Reservation, :count) }
    end
  end
end
