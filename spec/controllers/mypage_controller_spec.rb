# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MypageController, type: :controller do
  describe 'GET #index' do
    context 'when planner or customer is not signed in' do
      subject(:response) { get :index }

      it { expect(response).to redirect_to(root_path) }
    end

    context 'when planner is signed in' do
      let(:planner) { FactoryBot.create(:planner) }

      before do
        @request.env['devise.mapping'] = Devise.mappings[:planner]
        sign_in planner
      end
      subject(:response) { get :index }

      it { expect(response.status).to eq(200) }
    end

    context 'when customer is signed in' do
      let(:customer) { FactoryBot.create(:customer) }

      before do
        @request.env['devise.mapping'] = Devise.mappings[:customer]
        sign_in customer
      end
      subject(:response) { get :index }

      it { expect(response.status).to eq(200) }
    end
  end
end
