# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReservableFramesController, type: :controller do
  describe 'GET #index' do
    let(:planner) { FactoryBot.create(:planner) }

    before do
      @request.env['devise.mapping'] = Devise.mappings[:planner]
      sign_in planner
    end
    subject(:response) { get :index }

    it { expect(response.status).to eq(200) }
  end
end
