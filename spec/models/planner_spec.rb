# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Planner, type: :model do
  let(:planner) { FactoryBot.create(:planner) }

  it { expect(planner).to be_valid }

  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :email }
    it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
    it { is_expected.to validate_length_of(:password).is_at_least 8 }
    it { is_expected.to validate_length_of(:name).is_at_most 50 }
  end

  describe 'associations' do
    it { is_expected.to have_many(:reservable_frames).dependent(:destroy) }
    it { is_expected.to have_many(:reservations) }
  end
end
