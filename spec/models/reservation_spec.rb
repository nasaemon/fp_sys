# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Reservation, type: :model do
  let(:reservation) { FactoryBot.create(:reservation) }

  it { expect(reservation).to be_valid }

  describe 'validations' do
    subject { FactoryBot.create(:reservation) }
    it { is_expected.to validate_uniqueness_of :reservable_frame }
  end

  describe 'associations' do
    it { is_expected.to belong_to(:reservable_frame).required(true) }
    it { is_expected.to belong_to(:customer).required(true) }
  end
end
