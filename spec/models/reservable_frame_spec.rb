# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ReservableFrame, type: :model do
  let(:reservable_frame) { FactoryBot.create(:reservable_frame) }

  it { expect(reservable_frame).to be_valid }

  describe 'validations' do
    it { is_expected.to validate_presence_of :planner }
    it { is_expected.to validate_presence_of :time_frame }
    it { is_expected.to validate_presence_of :date }
  end

  describe 'callbacks' do
    it { is_expected.to callback(:validate_time_frame_is_enable).before(:validate) }
  end

  describe '#validate_time_frame_is_enable' do
    subject { reservable_frame.send(:validate_time_frame_is_enable) }

    context 'when date is nil' do
      before { reservable_frame.date = nil }
      it { expect { subject }.not_to change { reservable_frame.errors[:time_frame] }.from([]) }
    end

    context 'when date is not nil' do
      context 'when time_frame is nil' do
        before { reservable_frame.time_frame = nil }
        it { expect { subject }.not_to change { reservable_frame.errors[:time_frame] }.from([]) }
      end

      context 'when time_frame(date) is not enable' do
        before { reservable_frame.date = Date.new(2018, 6, 24) } # Sunday
        it { expect { subject }.to change { reservable_frame.errors[:time_frame] }.from([]).to(['有効な時間枠ではありません。']) }
      end

      context 'when time_frame(date) is enable' do
        before { reservable_frame.date = Date.new(2018, 6, 20) } # Weekday(Friday)
        it { expect { subject }.not_to change { reservable_frame.errors[:time_frame] }.from([]) }
      end
    end
  end

  describe 'associations' do
    it { is_expected.to belong_to(:planner) }
    it { is_expected.to have_one(:reservation).dependent(:destroy) }
  end
end
