# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TimeFrame, type: :model do
  let(:time_frame) { TimeFrame.find_by(start: start) }
  let(:start) { Tod::TimeOfDay.new(11, 0) }
  describe '#enable?' do
    subject { time_frame.enable?(date) }

    context 'date is nil' do
      let(:date) { nil }
      it { expect(subject).to be false }
    end

    context 'when date is weekday' do
      let(:date) { Date.new(2018, 6, 22) }
      it { expect(subject).to be true }
    end

    context 'when date is Saturday' do
      let(:date) { Date.new(2018, 6, 23) }
      context 'time_frame start is 10am' do
        let(:start) { Tod::TimeOfDay.new(10, 0) }
        it { expect(subject).to be false }
      end

      context 'when time_frame start is 11am' do
        it { expect(subject).to be true }
      end
    end

    context 'when date is Sunday' do
      let(:date) { Date.new(2018, 6, 24) }
      it { expect(subject).to be false }
    end
  end
end
