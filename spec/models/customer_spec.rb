# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Customer, type: :model do
  let(:customer) { FactoryBot.create(:customer) }

  it { expect(customer).to be_valid }
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :email }
    it { is_expected.to validate_uniqueness_of(:email).case_insensitive }
    it { is_expected.to validate_length_of(:password).is_at_least 8 }
    it { is_expected.to validate_length_of(:name).is_at_most 50 }
  end

  describe 'associations' do
    it { is_expected.to have_many(:reservations).dependent(:destroy) }
  end
end
