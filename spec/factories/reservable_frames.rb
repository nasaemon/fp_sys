# frozen_string_literal: true

FactoryBot.define do
  factory :reservable_frame do
    time_frame { TimeFrame.find(1) }
    planner { FactoryBot.create(:planner) }
    date '2018-06-19'
  end
end
