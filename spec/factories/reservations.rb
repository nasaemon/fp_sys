# frozen_string_literal: true

FactoryBot.define do
  factory :reservation do
    customer { FactoryBot.create(:customer) }
    reservable_frame { FactoryBot.create(:reservable_frame) }
  end
end
