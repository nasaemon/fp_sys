# frozen_string_literal: true

FactoryBot.define do
  factory :planner do
    name '市山 太郎'
    email 'ichiyama@example.com'
    password '09876543'
  end
end
