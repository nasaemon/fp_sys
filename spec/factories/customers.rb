# frozen_string_literal: true

FactoryBot.define do
  factory :customer do
    name '市川 智貴'
    email 'ichikawa@example.com'
    password '12345678'
  end
end
