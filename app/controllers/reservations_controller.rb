# frozen_string_literal: true

class ReservationsController < ApplicationController
  def index
    @planner = Planner.find(params[:planner_id])
    @time_frames = TimeFrame.all
    @dates = Time.zone.today.upto(Time.zone.today + 6.days)
    @reservable_frames = @planner.reservable_frames.reservable
  end

  def create
    @reservation = current_customer.reservations.build(reservation_params)
    if @reservation.save
      render json: @reservation
    else
      render json: @reservation.errors.full_messages, status: :unprocessable_entity
    end
  end

  private

  def reservation_params
    params.require(:reservation).permit(:reservable_frame_id)
  end
end
