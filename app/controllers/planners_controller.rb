# frozen_string_literal: true

class PlannersController < ApplicationController
  def index
    @planners = Planner.all
  end
end
