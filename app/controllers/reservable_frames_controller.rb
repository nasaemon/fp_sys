# frozen_string_literal: true

class ReservableFramesController < ApplicationController
  include DateConcern

  def index
    @dates = weekly_dates
    @reservable_frames = current_planner.reservable_frames.where(date: @dates.to_a)
    @time_frames = TimeFrame.all
  end

  def create
    @reservable_frame = current_planner.reservable_frames.build(reservable_frame_params)
    if @reservable_frame.save
      render json: @reservable_frame
    else
      render json: @reservable_frame.errors, status: :unprocessable_entity
    end
  end

  def destroy
    @reserved_frame = current_planner.reservable_frames.find(params[:id])
    @reserved_frame.destroy
  end

  private

  def reservable_frame_params
    params.require(:reservable_frame).permit(:time_frame_id, :date)
  end
end
