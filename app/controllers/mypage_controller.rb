# frozen_string_literal: true

class MypageController < ApplicationController
  include DateConcern

  before_action :authenticate_account!
  before_action :set_dates, only: %i[index]
  before_action :set_planner_reservations, only: %i[index], if: :planner_signed_in?
  before_action :set_customer_reservations, only: %i[index], if: :customer_signed_in?

  def index; end

  private

  def authenticate_account!
    redirect_to root_path unless customer_signed_in? || planner_signed_in?
  end

  def set_dates
    @dates = weekly_dates
  end

  def set_planner_reservations
    @planner_reservations =
      current_planner
      .reservations
      .eager_load(:customer)
      .where(reservable_frames: { date: @dates.to_a })
      .order(ReservableFrame.arel_table[:date],
             ReservableFrame.arel_table[:time_frame_id])
  end

  def set_customer_reservations
    @customer_reservations =
      current_customer
      .reservations
      .eager_load(reservable_frame: :planner)
      .where(reservable_frames: { date: @dates.to_a })
      .order(ReservableFrame.arel_table[:date],
             ReservableFrame.arel_table[:time_frame_id])
  end
end
