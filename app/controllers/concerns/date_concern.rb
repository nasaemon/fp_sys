# frozen_string_literal: true

module DateConcern
  private

  def weekly_dates
    Time.zone.today.upto(Time.zone.today + 6.days)
  end
end
