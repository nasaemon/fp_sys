document.addEventListener('turbolinks:load', function() {
  var $table = $('.js-reservable-frame-table');
  var reservableFrames = $table.data('reservable-frames');

  // 全体の枠（営業日外を除く）に対する処理
  $timeFrameTds = $table.find('.js-time-frame-td');
  $enableFrameTds = $timeFrameTds.filter('[data-enable="true"]');

  $enableFrameTds.each(function(index, cell) {
    $cell = $(cell);

    var date = $cell.data('date');
    var timeFrameId = $cell.data('frame-id');
    $cell.addClass('-unreservable');

    $cell.on('click', function() {
      $this = $(this);
      $reservable = $this.data('reservable');

      if($reservable) {
        id = $this.data('rfId');
        destroyAjaxRequest(id, date, timeFrameId, function(result){
          $this.addClass('-unreservable');
          $this.removeClass('-reservable');
          $this.data('reservable', false);
        });
      } else {
        createAjaxRequest(date, timeFrameId, function(result) {
          $this.addClass('-reservable');
          $this.removeClass('-unreservable');
          $this.data('reservable', true);
          $this.data('rfId', result.id);
        });
      }
    });
  });

  // 予約可能となっている枠に対する処理
  reservableFrames.forEach(function(frame) {
    var id = frame.id;
    var date = frame.date;
    var timeFrameId = frame.time_frame_id;
    var $cell = $table.find('[data-date=' + date + '][data-frame-id=' + timeFrameId  + ']');
    $cell.addClass('-reservable');
    $cell.removeClass('-unreservable');
    $cell.data('reservable', true);
    $cell.data('rfId', id);
  });

  // reservable_frames#create
  var createAjaxRequest = function(date, timeFrameId, callback) {
    $.ajax({
      url: '/reservable_frames/',
      type: 'POST',
      dataType: "json",
      data: {
        authenticity_token: $('[name="csrf-token"]')[0].content,
        reservable_frame: {
          date: date,
          time_frame_id: timeFrameId
        }
      }
    }).done(function(result) {
      if (callback) callback(result);
    }).fail(function(e) {
      alert('エラがー発生しました。');
    });
  };

  // reservable_frames#destroy
  var destroyAjaxRequest = function(id, date, timeFrameId, callback) {
    $.ajax({
      url: '/reservable_frames/' + id,
      type: 'DELETE',
      data: {
        authenticity_token: $('[name="csrf-token"]')[0].content
      }
    }).done(function(e) {
      if (callback) callback();
    }).fail(function(e) {
      alert('エラがー発生しました。');
    });
  };
});
