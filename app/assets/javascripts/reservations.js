document.addEventListener('turbolinks:load', function() {
  var $table = $('.js-reservations-table');
  var reservableFrames = $table.data('reservable-frames');

  // Customerが予約できる時間枠
  reservableFrames.forEach(function(frame) {
    var id = frame.id;
    var date = frame.date;
    var timeFrameId = frame.time_frame_id;
    var plannerId = frame.planner_id;
    var $cell = $table.find('[data-date=' + date + '][data-time-frame-id=' + timeFrameId + ']');
    $cell.addClass('-reservable');

    $cell.on('click', function() {
      $.ajax({
        url: '/planners/' + plannerId + '/reservations',
        type: 'POST',
        dataType: "json",
        data: {
          authenticity_token: $('[name="csrf-token"]')[0].content,
          reservation: {
            reservable_frame_id: id
          }
        }
      }).done(function(result) {
        $cell.off('click');
        $cell.removeClass('-reservable');
      }).fail(function(jqXHR) {
        var errorMessage = 'エラーが発生しました。';

        var response = jqXHR.responseJSON;
        // Railsから帰ってくるエラー(JSON)を文にする。
        jqXHR.responseJSON.forEach(function(message) {
          errorMessage += "\n" + message;
        });
        alert(errorMessage);
      });
    });
  });
});
