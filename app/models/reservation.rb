# frozen_string_literal: true

class Reservation < ApplicationRecord
  belongs_to :customer
  belongs_to :reservable_frame

  validates :reservable_frame, uniqueness: true
end
