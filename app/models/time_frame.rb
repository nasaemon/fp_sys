# frozen_string_literal: true

class TimeFrame < ActiveHash::Base
  START_TIME_OF_DAY = Tod::TimeOfDay.new(10, 0).freeze #  10:00~
  END_TIME_OF_DAY = Tod::TimeOfDay.new(18, 0).freeze   # ~18:00
  SEC_PER_FRAME = 1800 # 30 minutes
  FRAMES_PER_DAY = Tod::Shift.new(START_TIME_OF_DAY, END_TIME_OF_DAY).duration \
                  / SEC_PER_FRAME.freeze
  SAT_START_TIME_OF_DAY = Tod::TimeOfDay.new(11, 0) #  11:00~
  SAT_END_TIME_OF_DAY = Tod::TimeOfDay.new(15, 0)   # ~15:00

  #
  # START_TIME_OF_DAYからEND_TIME_OF_DAYまでの全枠(FRAMES_PER_DAY毎)をdataとして格納。
  #
  # start: その枠の開始時間
  #   end: その枠の終了時間
  #
  self.data = Array.new(FRAMES_PER_DAY) do |i|
    {
      id: i + 1,
      start: START_TIME_OF_DAY + SEC_PER_FRAME * i,
      end: START_TIME_OF_DAY + SEC_PER_FRAME * (i + 1)
    }
  end

  def self.first_on_saturday
    find_by(start: SAT_START_TIME_OF_DAY)
  end

  def self.last_on_saturday
    find_by(end: SAT_END_TIME_OF_DAY)
  end

  def enable?(date)
    if date.nil?
      false
    elsif date.saturday?
      start_idx = TimeFrame.first_on_saturday.id
      end_idx = TimeFrame.last_on_saturday.id
      (start_idx..end_idx).cover?(id)
    elsif date.sunday?
      false
    else
      true
    end
  end
end
