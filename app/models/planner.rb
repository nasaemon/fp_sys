# frozen_string_literal: true

class Planner < User
  has_many :reservable_frames, dependent: :destroy
  has_many :reservations, through: :reservable_frames
end
