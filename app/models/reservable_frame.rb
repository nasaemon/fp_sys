# frozen_string_literal: true

class ReservableFrame < ApplicationRecord
  extend ActiveHash::Associations::ActiveRecordExtensions

  has_one :reservation, dependent: :destroy
  belongs_to_active_hash :time_frame
  belongs_to :planner

  validates :planner, presence: true
  validates :time_frame, presence: true
  validates :date, presence: true

  validate :validate_time_frame_is_enable

  scope :reservable, -> { left_joins(:reservation).where(Reservation.arel_table[:id].eq(nil)) }

  private

  def validate_time_frame_is_enable
    return if date.nil?
    return if time_frame.nil?

    errors.add(:time_frame, '有効な時間枠ではありません。') unless time_frame.enable?(date)
  end
end
