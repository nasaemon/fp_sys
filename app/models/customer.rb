# frozen_string_literal: true

class Customer < User
  has_many :reservations, dependent: :destroy
end
